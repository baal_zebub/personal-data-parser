package code.ksmardze.pdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalDataParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalDataParserApplication.class, args);
	}

}
