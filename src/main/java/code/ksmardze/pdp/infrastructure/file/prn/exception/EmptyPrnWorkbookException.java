package code.ksmardze.pdp.infrastructure.file.prn.exception;

public class EmptyPrnWorkbookException extends PrnWorkbookParsingException {

	public EmptyPrnWorkbookException() {
		super("File is empty");
	}
}
