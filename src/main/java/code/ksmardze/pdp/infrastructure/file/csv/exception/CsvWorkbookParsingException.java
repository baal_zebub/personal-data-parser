package code.ksmardze.pdp.infrastructure.file.csv.exception;

import code.ksmardze.pdp.ui.exception.WorkbookParsingException;

public abstract class CsvWorkbookParsingException extends WorkbookParsingException {

	CsvWorkbookParsingException(String message) {
		super(message);
	}
}
