package code.ksmardze.pdp.infrastructure.file.csv.exception;

public class InvalidCsvWorkbookValueException extends CsvWorkbookParsingException {

	public InvalidCsvWorkbookValueException(String fieldName, String value) {
		super("invalid value \"" + value + "\" of field \"" + fieldName + "\"");
	}
}
