package code.ksmardze.pdp.infrastructure.file;

import java.util.Map;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;
import code.ksmardze.pdp.domain.WorkbookColumn;
import code.ksmardze.pdp.infrastructure.file.csv.exception.InvalidCsvWorkbookRowException;
import code.ksmardze.pdp.infrastructure.file.prn.PrnField;

public class CreatePersonalDataCommandFactory {

	private static final int EXPECTED_NUMBER_OF_FIELDS = WorkbookColumn.values().length;

	public static CreatePersonalDataCommand create(String[] csvRow) {
		if (csvRow.length != EXPECTED_NUMBER_OF_FIELDS) {
			throw new InvalidCsvWorkbookRowException("Invalid row: " + String.join(";", csvRow));
		}

		return new CreatePersonalDataCommandImpl(csvRow[0], csvRow[1], csvRow[2], csvRow[3], csvRow[4], csvRow[5]);
	}

	public static CreatePersonalDataCommand create(String prnRow, Map<String, PrnField> columnNamesToFields) {
		return new CreatePersonalDataCommandImpl(
			fieldValue("Name", prnRow, columnNamesToFields),
			fieldValue("Address", prnRow, columnNamesToFields),
			fieldValue("Postcode", prnRow, columnNamesToFields),
			fieldValue("Phone", prnRow, columnNamesToFields),
			fieldValue("Credit Limit", prnRow, columnNamesToFields),
			fieldValue("Birthday", prnRow, columnNamesToFields)
		);
	}

	private static String fieldValue(String columnName, String row, Map<String, PrnField> columnNamesToFields) {
		return row.substring(columnNamesToFields.get(columnName).start(), columnNamesToFields.get(columnName).end())
			.trim();
	}
}
