package code.ksmardze.pdp.infrastructure.file.prn.exception;

import code.ksmardze.pdp.ui.exception.WorkbookParsingException;

public abstract class PrnWorkbookParsingException extends WorkbookParsingException {

	PrnWorkbookParsingException(String message) {
		super(message);
	}
}
