package code.ksmardze.pdp.infrastructure.file.csv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;
import code.ksmardze.pdp.infrastructure.file.CreatePersonalDataCommandFactory;
import code.ksmardze.pdp.domain.WorkbookColumn;
import code.ksmardze.pdp.infrastructure.file.csv.exception.EmptyCsvWorkbookHeaderException;
import code.ksmardze.pdp.infrastructure.file.csv.exception.MissingCsvWorkbookColumnException;

public class OpencsvWorkbookParserWrapper implements CsvWorkbookParser {

	private final InputStream input;

	OpencsvWorkbookParserWrapper(InputStream input) {
		this.input = input;
	}

	@Override
	public List<? extends CreatePersonalDataCommand> parse() throws IOException, CsvValidationException {
		List<CreatePersonalDataCommand> result = new ArrayList<>();

		CSVReader reader = new CSVReader(new InputStreamReader(input, StandardCharsets.ISO_8859_1));
		String[] row =reader.readNext();

		validateHeader(row);

		while ((row = reader.readNext()) != null) {
			result.add(CreatePersonalDataCommandFactory.create(row));
		}

		return result;
	}

	private void validateHeader(String[] row) {
		if (row == null || row.length == 0) {
			throw new EmptyCsvWorkbookHeaderException();
		}
		Arrays.stream(WorkbookColumn.values())
			.map(WorkbookColumn::columnName)
			.forEach(columnName -> verifyColumnPresenceInHeader(row, columnName));
	}

	private void verifyColumnPresenceInHeader(String[] row, String columnName) {
		if (!Arrays.asList(row).contains(columnName)) {
			throw new MissingCsvWorkbookColumnException(String.join(";", row), columnName);
		}
	}
}
