package code.ksmardze.pdp.infrastructure.file.prn;

import static code.ksmardze.pdp.domain.WorkbookColumn.BIRTHDAY;
import static code.ksmardze.pdp.domain.WorkbookColumn.CREDIT_LIMIT;
import static code.ksmardze.pdp.domain.WorkbookColumn.PHONE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

import com.google.common.collect.ImmutableList;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;
import code.ksmardze.pdp.domain.WorkbookColumn;
import code.ksmardze.pdp.infrastructure.file.CreatePersonalDataCommandFactory;
import code.ksmardze.pdp.infrastructure.file.prn.exception.EmptyPrnWorkbookException;
import code.ksmardze.pdp.infrastructure.file.prn.exception.MissingPrnHeaderColumnException;

public class PrnWorkbookParserImpl implements PrnWorkbookParser {

	private static final String PHONE_NUMBER_PATTERN = "\\+?([0-9]+[- ]?)+[0-9]{2,}";
	private static final String CREDIT_LIMIT_PATTERN = "\\d+";

	private static final List<String> COLUMN_NAMES = Arrays.stream(WorkbookColumn.values())
		.map(WorkbookColumn::columnName)
		.collect(ImmutableList.toImmutableList());

	private InputStream input;

	PrnWorkbookParserImpl(InputStream input) {
		this.input = input;
	}

	public List<? extends CreatePersonalDataCommand> parse() throws IOException, MissingPrnHeaderColumnException {
		final List<String> lines;

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.ISO_8859_1))) {
			lines = reader.lines()
				.collect(ImmutableList.toImmutableList());
		}

		if(lines.isEmpty()) {
			throw new EmptyPrnWorkbookException();
		}

		final String header = lines.get(0);
		validateHeader(header);
		final List<String> rowsWithoutHeader = lines.subList(1, lines.size());

		Map<String, PrnField> columnNamesToFields = new HashMap<>();
		COLUMN_NAMES
			.forEach(columnName -> columnNamesToFields.put(columnName, field(header, rowsWithoutHeader, columnName)));

		return rowsWithoutHeader.stream()
			.map(row -> CreatePersonalDataCommandFactory.create(row, columnNamesToFields))
			.collect(ImmutableList.toImmutableList());
	}

	private void validateHeader(String header) {
		COLUMN_NAMES
			.forEach(columnName -> verifyColumnPresenceInHeader(header, columnName));
	}

	private PrnField field(String header, List<String> rows, String columnName) {
		final int start;
		final int end;

		if (columnName.equals(PHONE.columnName())) {
			start = header.indexOf(columnName);
			end = leftAlignedColumnEnd(rows, PHONE_NUMBER_PATTERN, start);
		} else if (columnName.equals(CREDIT_LIMIT.columnName())) {
			end = header.indexOf(columnName) + columnName.length();
			start = rightAlignedColumnStart(rows, CREDIT_LIMIT_PATTERN, end);
		}
		else if (columnName.equals(BIRTHDAY.columnName())) {
			start = header.indexOf(columnName);
			end = header.length();
		} else {
			start = header.indexOf(columnName);
			end = header.indexOf(COLUMN_NAMES.get(COLUMN_NAMES.indexOf(columnName) + 1));
		}

		return new PrnField(columnName, start, end);
	}

	private void verifyColumnPresenceInHeader(String header, String columnName) {
		if (!header.contains(columnName)) {
			throw new MissingPrnHeaderColumnException(header, columnName);
		}
	}

	private int leftAlignedColumnEnd(List<String> rows, String valuePattern, int columnStart) {
		return rows.stream()
			.mapToInt(row -> leftAlignedColumnValueEnd(row, valuePattern, columnStart))
			.max()
			.orElseThrow(NoSuchElementException::new);
	}

	private int rightAlignedColumnStart(List<String> rows, String valuePattern, int columnEnd) {
		return rows.stream()
			.mapToInt(row -> rightAlignedColumnValueStart(row, valuePattern, columnEnd))
			.min()
			.orElseThrow(NoSuchElementException::new);
	}

	private int leftAlignedColumnValueEnd(String row, String valuePattern, int columnStart) {
		final String rowFromColumnStart = row.substring(columnStart);
		Matcher matcher = Pattern.compile(valuePattern).matcher(rowFromColumnStart);
		boolean columnValueFound = false;

		while (!columnValueFound && matcher.find()) {
			if (matcher.start() == 0) {
				columnValueFound = true;
			}
		}

		Assert.isTrue(columnValueFound, "no value found, something went wrong");

		return columnStart + matcher.end();
	}

	private int rightAlignedColumnValueStart(String row, String valuePattern, int columnEnd) {
		Matcher matcher = Pattern.compile(valuePattern).matcher(row);
		boolean columnValueFound = false;

		while (!columnValueFound && matcher.find()) {
			if (matcher.end() == columnEnd) {
				columnValueFound = true;
			}
		}

		Assert.isTrue(columnValueFound, "no value found, something went wrong");

		return matcher.start();
	}
}
