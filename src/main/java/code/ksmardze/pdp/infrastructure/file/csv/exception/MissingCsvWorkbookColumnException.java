package code.ksmardze.pdp.infrastructure.file.csv.exception;

public class MissingCsvWorkbookColumnException extends CsvWorkbookParsingException {
	public MissingCsvWorkbookColumnException(String header, String columnName) {
		super("Missing column \"" + columnName + "\" in header \"" + header + "\"");
	}
}
