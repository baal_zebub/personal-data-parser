package code.ksmardze.pdp.infrastructure.file;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;

public class CreatePersonalDataCommandImpl implements CreatePersonalDataCommand {

	private final String name;
	private final String address;
	private final String postcode;
	private final String phone;
	private final String creditLimit;
	private final String birthday;

	CreatePersonalDataCommandImpl(String name, String address, String postcode, String phone, String creditLimit, String birthday) {
		this.name = name;
		this.address = address;
		this.postcode = postcode;
		this.phone = phone;
		this.creditLimit = creditLimit;
		this.birthday = birthday;
	}

	public String name() {
		return name;
	}

	@Override
	public String address() {
		return address;
	}

	@Override
	public String postcode() {
		return postcode;
	}

	@Override
	public String phone() {
		return phone;
	}

	@Override
	public String creditLimit() {
		return creditLimit;
	}

	@Override
	public String birthday() {
		return birthday;
	}
}
