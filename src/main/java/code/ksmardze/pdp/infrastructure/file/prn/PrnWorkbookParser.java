package code.ksmardze.pdp.infrastructure.file.prn;

import java.io.IOException;
import java.util.List;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;

public interface PrnWorkbookParser {

	List<? extends CreatePersonalDataCommand> parse() throws IOException;
}
