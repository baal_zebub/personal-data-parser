package code.ksmardze.pdp.infrastructure.file.prn;

import java.io.InputStream;

public class PrnWorkbookParserFactory {

	public static PrnWorkbookParser create(InputStream input) {
		return new PrnWorkbookParserImpl(input);
	}
}
