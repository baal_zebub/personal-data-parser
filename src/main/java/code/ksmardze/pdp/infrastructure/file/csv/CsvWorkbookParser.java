package code.ksmardze.pdp.infrastructure.file.csv;

import java.io.IOException;
import java.util.List;

import com.opencsv.exceptions.CsvValidationException;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;

public interface CsvWorkbookParser {

	List<? extends CreatePersonalDataCommand> parse() throws IOException, CsvValidationException;
}
