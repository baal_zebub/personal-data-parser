package code.ksmardze.pdp.infrastructure.file.csv.exception;

public class GeneralCsvWorkbookParsingException extends CsvWorkbookParsingException {

	public GeneralCsvWorkbookParsingException(String message) {
		super(message);
	}
}
