package code.ksmardze.pdp.infrastructure.file.csv;

import java.io.InputStream;

public class CsvWorkbookParserFactory {

	public static CsvWorkbookParser create(InputStream input) {
		return new OpencsvWorkbookParserWrapper(input);
	}
}
