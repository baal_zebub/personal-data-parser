package code.ksmardze.pdp.infrastructure.file.strategy;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;
import com.opencsv.exceptions.CsvValidationException;

import code.ksmardze.pdp.application.command.WorkbookParsingStrategy;
import code.ksmardze.pdp.domain.PersonalData;
import code.ksmardze.pdp.infrastructure.file.csv.CsvWorkbookParserFactory;

@Component
public class CsvWorkbookParsingStrategy implements WorkbookParsingStrategy {

	@Override
	public List<PersonalData> parse(InputStream input) throws IOException, CsvValidationException {
		return CsvWorkbookParserFactory.create(input).parse().stream()
			.map(PersonalData::new)
			.collect(ImmutableList.toImmutableList());
	}
}
