package code.ksmardze.pdp.infrastructure.file.prn.exception;

public class MissingPrnHeaderColumnException extends PrnWorkbookParsingException {

	public MissingPrnHeaderColumnException(String header, String columnName) {
		super("Missing column " + columnName + " in header \"" + header + "\"");
	}
}
