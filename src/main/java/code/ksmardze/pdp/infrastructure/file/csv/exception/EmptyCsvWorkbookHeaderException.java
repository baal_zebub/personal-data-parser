package code.ksmardze.pdp.infrastructure.file.csv.exception;

public class EmptyCsvWorkbookHeaderException extends CsvWorkbookParsingException {

	public EmptyCsvWorkbookHeaderException() {
		super("File header is empty");
	}
}
