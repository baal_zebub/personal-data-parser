package code.ksmardze.pdp.infrastructure.file.prn;

import org.springframework.util.Assert;

public class PrnField {

	private final int start;
	private final int end;

	public PrnField(String name, int start, int end) {
		Assert.isTrue(start >= 0 && start < end, "invalid character range " + start + "-" + end + " for field " + name);
		this.start = start;
		this.end = end;
	}

	public int start() {
		return start;
	}

	public int end() {
		return end;
	}
}
