package code.ksmardze.pdp.infrastructure.file.strategy;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import code.ksmardze.pdp.application.command.WorkbookParsingStrategy;
import code.ksmardze.pdp.domain.PersonalData;
import code.ksmardze.pdp.infrastructure.file.prn.PrnWorkbookParserFactory;
import code.ksmardze.pdp.infrastructure.file.prn.exception.MissingPrnHeaderColumnException;

@Component
public class PrnWorkbookParsingStrategy implements WorkbookParsingStrategy {

	@Override
	public List<PersonalData> parse(InputStream input) throws IOException, MissingPrnHeaderColumnException {
		return PrnWorkbookParserFactory.create(input).parse().stream()
			.map(PersonalData::new)
			.collect(ImmutableList.toImmutableList());
	}
}
