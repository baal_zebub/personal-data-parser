package code.ksmardze.pdp.infrastructure.file.csv.exception;

public class InvalidCsvWorkbookRowException extends CsvWorkbookParsingException {

	public InvalidCsvWorkbookRowException(String message) {
		super(message);
	}
}
