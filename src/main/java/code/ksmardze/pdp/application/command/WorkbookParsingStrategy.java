package code.ksmardze.pdp.application.command;

import java.io.InputStream;
import java.util.List;

import code.ksmardze.pdp.domain.PersonalData;

public interface WorkbookParsingStrategy {
	List<PersonalData> parse(InputStream input) throws Exception;
}
