package code.ksmardze.pdp.domain;

public class PersonalData {

	private final String name;
	private final String address;
	private final String postcode;
	private final String phone;
	private final String creditLimit;
	private final String birthday;

	public PersonalData(CreatePersonalDataCommand command) {
		this.name = command.name();
		this.address = command.address();
		this.postcode = command.postcode();
		this.phone = command.phone();
		this.creditLimit = command.creditLimit();
		this.birthday = command.birthday();
	}

	public String name() {
		return name;
	}

	public String address() {
		return address;
	}

	public String postcode() {
		return postcode;
	}

	public String phone() {
		return phone;
	}

	public String creditLimit() {
		return creditLimit;
	}

	public String birthday() {
		return birthday;
	}
}
