package code.ksmardze.pdp.domain;

public enum WorkbookColumn {

	NAME("Name"),
	ADRESS("Address"),
	POSTCODE("Postcode"),
	PHONE("Phone"),
	CREDIT_LIMIT("Credit Limit"),
	BIRTHDAY("Birthday");

	private final String columnName;

	WorkbookColumn(String columnName) {
		this.columnName = columnName;
	}

	public String columnName() {
		return columnName;
	}
}
