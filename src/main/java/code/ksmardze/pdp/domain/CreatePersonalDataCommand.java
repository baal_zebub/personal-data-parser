package code.ksmardze.pdp.domain;

public interface CreatePersonalDataCommand {

	String name();
	String address();
	String postcode();
	String phone();
	String creditLimit();
	String birthday();
}
