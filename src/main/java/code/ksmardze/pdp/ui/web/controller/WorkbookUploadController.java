package code.ksmardze.pdp.ui.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import code.ksmardze.pdp.domain.PersonalData;
import code.ksmardze.pdp.ui.controller.WorkbookFormat;
import code.ksmardze.pdp.ui.controller.WorkbookParsingStrategyChooser;
import code.ksmardze.pdp.ui.exception.WorkbookParsingException;

@Controller
public class WorkbookUploadController {

	@Autowired
	private WorkbookParsingStrategyChooser parsingStrategyChooser;

	@GetMapping("/")
	public String uploadForm() {
		return "upload-form";
	}

	@PostMapping("/upload-workbook")
	public String uploadWorkbook(@RequestParam("workbook") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception {
		final WorkbookFormat format = WorkbookFormat.findForFilename(file.getOriginalFilename());
		final List<PersonalData> workbookRecords = parsingStrategyChooser.chooseBasedOnFormat(format)
			.parse(file.getInputStream());

		redirectAttributes.addFlashAttribute("filename", file.getOriginalFilename());
		redirectAttributes.addFlashAttribute("records", workbookRecords);
		redirectAttributes.addFlashAttribute("message",
			"You successfully uploaded " + file.getOriginalFilename() + "!");

		return "redirect:/";
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(WorkbookParsingException.class)
	public ModelAndView handleParsingException(Exception exception) {
		return modelAndViewForExceptionHandling(exception);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exception) {
		return modelAndViewForExceptionHandling(exception);
	}

	private ModelAndView modelAndViewForExceptionHandling(Exception exception) {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("errorMessage", exception.getMessage());
		modelAndView.setViewName("upload-form");

		return modelAndView;
	}
}
