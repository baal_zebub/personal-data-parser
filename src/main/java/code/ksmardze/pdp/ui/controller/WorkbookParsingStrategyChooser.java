package code.ksmardze.pdp.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import code.ksmardze.pdp.application.command.WorkbookParsingStrategy;
import code.ksmardze.pdp.ui.exception.WorkbookParsingStrategyNotFoundException;

@Component
public class WorkbookParsingStrategyChooser {

	@Autowired
	@Qualifier("csvWorkbookParsingStrategy")
	private WorkbookParsingStrategy csvWorkbookParsingStrategy;

	@Autowired
	@Qualifier("prnWorkbookParsingStrategy")
	private WorkbookParsingStrategy prnWorkbookParsingStrategy;

	public WorkbookParsingStrategy chooseBasedOnFormat(WorkbookFormat format) {
		switch (format) {
			case CSV:
				return csvWorkbookParsingStrategy;
			case PRN:
				return prnWorkbookParsingStrategy;
			default:
				throw new WorkbookParsingStrategyNotFoundException(format);
		}
	}
}
