package code.ksmardze.pdp.ui.controller.exception;

import code.ksmardze.pdp.ui.exception.WorkbookParsingException;

public class UnsupportedWorkbookFormatException extends WorkbookParsingException {

	public UnsupportedWorkbookFormatException() {
		super("Unsupported format, supported formats are csv and prn");
	}
}
