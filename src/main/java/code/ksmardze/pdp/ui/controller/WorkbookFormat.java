package code.ksmardze.pdp.ui.controller;

import java.util.Arrays;

import code.ksmardze.pdp.ui.controller.exception.UnsupportedWorkbookFormatException;

public enum WorkbookFormat {

	CSV(".csv"),
	PRN(".prn");

	private final String fileExtension;

	WorkbookFormat(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public static WorkbookFormat findForFilename(String filename) {
		return Arrays.stream(values())
			.filter(format -> filename.endsWith(format.fileExtension))
			.findFirst()
			.orElseThrow(UnsupportedWorkbookFormatException::new);
	}
}
