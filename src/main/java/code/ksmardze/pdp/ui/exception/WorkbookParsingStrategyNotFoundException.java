package code.ksmardze.pdp.ui.exception;

import code.ksmardze.pdp.ui.controller.WorkbookFormat;

public class WorkbookParsingStrategyNotFoundException extends RuntimeException {
	public WorkbookParsingStrategyNotFoundException(WorkbookFormat format) {
		super("No parsing strategy defined for " + format);
	}
}
