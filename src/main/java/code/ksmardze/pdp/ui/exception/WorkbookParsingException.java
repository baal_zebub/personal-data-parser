package code.ksmardze.pdp.ui.exception;

public abstract class WorkbookParsingException extends RuntimeException {

	public WorkbookParsingException(String message) {
		super(message);
	}
}
