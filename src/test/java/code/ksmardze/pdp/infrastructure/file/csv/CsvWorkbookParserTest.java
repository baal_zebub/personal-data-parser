package code.ksmardze.pdp.infrastructure.file.csv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.opencsv.exceptions.CsvValidationException;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;
import code.ksmardze.pdp.domain.assertions.CreatePersonalDataCommandAssert;
import code.ksmardze.pdp.infrastructure.file.csv.exception.CsvWorkbookParsingException;

class CsvWorkbookParserTest {

	@Test
	void shouldParseValidCsvFile() throws IOException, CsvValidationException {
		InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/csv/Workbook2.csv"));

		final List<? extends CreatePersonalDataCommand> result = CsvWorkbookParserFactory.create(input).parse();

		assertThat(result).hasSize(7);

		CreatePersonalDataCommandAssert.assertThat(result.get(0))
			.hasName("Johnson, John")
			.hasAddress("Voorstraat 32")
			.hasPostcode("3122gg")
			.hasPhone("020 3849381")
			.hasCreditLimit("10000")
			.hasBirthday("01/01/1987");

		CreatePersonalDataCommandAssert.assertThat(result.get(1))
			.hasName("Anderson, Paul")
			.hasAddress("Dorpsplein 3A")
			.hasPostcode("4532 AA")
			.hasPhone("030 3458986")
			.hasCreditLimit("109093")
			.hasBirthday("03/12/1965");

		CreatePersonalDataCommandAssert.assertThat(result.get(2))
			.hasName("Wicket, Steve")
			.hasAddress("Mendelssohnstraat 54d")
			.hasPostcode("3423 ba")
			.hasPhone("0313-398475")
			.hasCreditLimit("934")
			.hasBirthday("03/06/1964");

		CreatePersonalDataCommandAssert.assertThat(result.get(3))
			.hasName("Benetar, Pat")
			.hasAddress("Driehoog 3zwart")
			.hasPostcode("2340 CC")
			.hasPhone("06-28938945")
			.hasCreditLimit("54")
			.hasBirthday("04/09/1964");

		CreatePersonalDataCommandAssert.assertThat(result.get(4))
			.hasName("Gibson, Mal")
			.hasAddress("Vredenburg 21")
			.hasPostcode("3209 DD")
			.hasPhone("06-48958986")
			.hasCreditLimit("54.5")
			.hasBirthday("09/11/1978");

		CreatePersonalDataCommandAssert.assertThat(result.get(5))
			.hasName("Friendly, User")
			.hasAddress("Sint Jansstraat 32")
			.hasPostcode("4220 EE")
			.hasPhone("0885-291029")
			.hasCreditLimit("63.6")
			.hasBirthday("10/08/1980");

		CreatePersonalDataCommandAssert.assertThat(result.get(6))
			.hasName("Smith, John")
			.hasAddress("Børkestraße 32")
			.hasPostcode("87823")
			.hasPhone("+44 728 889838")
			.hasCreditLimit("9898.3")
			.hasBirthday("20/09/1999");
	}

	@Test
	void shouldThrowExceptionOnEmptyFile() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/csv/EmptyWorkbook2.csv"));

		assertThatExceptionOfType(CsvWorkbookParsingException.class)
			.isThrownBy(() -> CsvWorkbookParserFactory.create(input).parse());
	}

	@Test
	void shouldExceptionOnMissingHeader() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/csv/NoHeaderWorkbook2.csv"));

		assertThatExceptionOfType(CsvWorkbookParsingException.class)
			.isThrownBy(() -> CsvWorkbookParserFactory.create(input).parse());
	}

	@Test
	void shouldExceptionOnHeaderDelimiterOtherThanComma() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/csv/Workbook2.ssv"));

		assertThatExceptionOfType(CsvWorkbookParsingException.class)
			.isThrownBy(() -> CsvWorkbookParserFactory.create(input).parse());
	}

	@Test
	void shouldThrowExceptionOnRowDelimiterOtherThanComma() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/csv/Workbook2WithSemicolonInDataRow.csv"));

		assertThatExceptionOfType(CsvWorkbookParsingException.class)
			.isThrownBy(() -> CsvWorkbookParserFactory.create(input).parse());
	}
}