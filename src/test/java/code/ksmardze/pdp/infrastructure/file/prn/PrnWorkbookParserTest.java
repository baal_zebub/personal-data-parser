package code.ksmardze.pdp.infrastructure.file.prn;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;
import code.ksmardze.pdp.domain.assertions.CreatePersonalDataCommandAssert;
import code.ksmardze.pdp.infrastructure.file.prn.exception.PrnWorkbookParsingException;

class PrnWorkbookParserTest {

	@Test
	void shouldParseValidFile() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/prn/Workbook2.prn"));

		PrnWorkbookParser parser = PrnWorkbookParserFactory.create(input);

		final List<? extends CreatePersonalDataCommand> result = parser.parse();

		assertThat(result).hasSize(7);

		CreatePersonalDataCommandAssert.assertThat(result.get(0))
			.hasName("Johnson, John")
			.hasAddress("Voorstraat 32")
			.hasPostcode("3122gg")
			.hasPhone("020 3849381")
			.hasCreditLimit("1000000")
			.hasBirthday("19870101");

		CreatePersonalDataCommandAssert.assertThat(result.get(1))
			.hasName("Anderson, Paul")
			.hasAddress("Dorpsplein 3A")
			.hasPostcode("4532 AA")
			.hasPhone("030 3458986")
			.hasCreditLimit("10909300")
			.hasBirthday("19651203");

		CreatePersonalDataCommandAssert.assertThat(result.get(2))
			.hasName("Wicket, Steve")
			.hasAddress("Mendelssohnstraat 54d")
			.hasPostcode("3423 ba")
			.hasPhone("0313-398475")
			.hasCreditLimit("93400")
			.hasBirthday("19640603");

		CreatePersonalDataCommandAssert.assertThat(result.get(3))
			.hasName("Benetar, Pat")
			.hasAddress("Driehoog 3zwart")
			.hasPostcode("2340 CC")
			.hasPhone("06-28938945")
			.hasCreditLimit("54")
			.hasBirthday("19640904");

		CreatePersonalDataCommandAssert.assertThat(result.get(4))
			.hasName("Gibson, Mal")
			.hasAddress("Vredenburg 21")
			.hasPostcode("3209 DD")
			.hasPhone("06-48958986")
			.hasCreditLimit("5450")
			.hasBirthday("19781109");

		CreatePersonalDataCommandAssert.assertThat(result.get(5))
			.hasName("Friendly, User")
			.hasAddress("Sint Jansstraat 32")
			.hasPostcode("4220 EE")
			.hasPhone("0885-291029")
			.hasCreditLimit("6360")
			.hasBirthday("19800810");

		CreatePersonalDataCommandAssert.assertThat(result.get(6))
			.hasName("Smith, John")
			.hasAddress("Børkestraße 32")
			.hasPostcode("87823")
			.hasPhone("+44 728 889838")
			.hasCreditLimit("989830")
			.hasBirthday("19990920");
	}

	@Test
	void shouldParseRowWithBlankName() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/prn/BlankNameWorkbook2.prn"));

		PrnWorkbookParser parser = PrnWorkbookParserFactory.create(input);

		final List<? extends CreatePersonalDataCommand> result = parser.parse();

		assertThat(result).hasSize(1);
		CreatePersonalDataCommandAssert.assertThat(result.get(0))
			.hasName("")
			.hasAddress("Voorstraat 32")
			.hasPostcode("3122gg")
			.hasPhone("020 3849381")
			.hasCreditLimit("1000000")
			.hasBirthday("19870101");
	}

	@Test
	void shouldThrowExceptionOnEmptyFile() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/prn/EmptyWorkbook2.prn"));

		PrnWorkbookParser parser = PrnWorkbookParserFactory.create(input);

		final Throwable throwable = catchThrowable(parser::parse);

		assertThat(throwable).isInstanceOf(PrnWorkbookParsingException.class);
	}

	@Test
	void shouldThrowExceptionOnMissingHeaderColumn() throws IOException {
		final InputStream input = Files.newInputStream(Paths.get("src/test/resources/samples/prn/MissingPostcodeHeaderColumnWorkbook2.prn"));

		PrnWorkbookParser parser = PrnWorkbookParserFactory.create(input);

		final Throwable throwable = catchThrowable(parser::parse);

		assertThat(throwable).isInstanceOf(PrnWorkbookParsingException.class);
	}
}