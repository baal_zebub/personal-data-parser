package code.ksmardze.pdp.domain.assertions;

import java.util.Objects;

import org.assertj.core.api.AbstractAssert;

import code.ksmardze.pdp.domain.CreatePersonalDataCommand;

public class CreatePersonalDataCommandAssert extends AbstractAssert<CreatePersonalDataCommandAssert, CreatePersonalDataCommand> {

	private static final String ERROR_MESSAGE_PREFIX = "Expected ";
	private static final String ERROR_MESSAGE_SUFFIX = " to be <%s> but was <%s>";

	private CreatePersonalDataCommandAssert(CreatePersonalDataCommand actual) {
		super(actual, CreatePersonalDataCommandAssert.class);
	}

	public static CreatePersonalDataCommandAssert assertThat(CreatePersonalDataCommand actual) {
		return new CreatePersonalDataCommandAssert(actual);
	}

	public CreatePersonalDataCommandAssert hasName(String name) {
		isNotNull();

		if(!Objects.equals(actual.name(), name)) {
			failWithMessage(getErrorMessage("name"), name, actual.name());
		}

		return this;
	}

	public CreatePersonalDataCommandAssert hasAddress(String address) {
		isNotNull();

		if(!Objects.equals(actual.address(), address)) {
			failWithMessage(getErrorMessage("address"), address, actual.address());
		}

		return this;
	}

	public CreatePersonalDataCommandAssert hasPostcode(String postcode) {
		isNotNull();

		if(!Objects.equals(actual.postcode(), postcode)) {
			failWithMessage(getErrorMessage("postcode"), postcode, actual.postcode());
		}

		return this;
	}

	public CreatePersonalDataCommandAssert hasPhone(String phone) {
		isNotNull();

		if(!Objects.equals(actual.phone(), phone)) {
			failWithMessage(getErrorMessage("phone"), phone, actual.phone());
		}

		return this;
	}

	public CreatePersonalDataCommandAssert hasCreditLimit(String creditLimit) {
		isNotNull();

		if(!Objects.equals(actual.creditLimit(), creditLimit)) {
			failWithMessage(getErrorMessage("credit limit"), creditLimit, actual.creditLimit());
		}

		return this;
	}

	public CreatePersonalDataCommandAssert hasBirthday(String birthday) {
		isNotNull();

		if(!Objects.equals(actual.birthday(), birthday)) {
			failWithMessage(getErrorMessage("birthday"), birthday, actual.birthday());
		}

		return this;
	}

	private String getErrorMessage(String fieldDescription) {
		return ERROR_MESSAGE_PREFIX + fieldDescription + ERROR_MESSAGE_SUFFIX;
	}
}
