# Running

1. Start the app
     * from source: `mvn spring-boot:run`
     * or from fat jar: `java -jar personal-data-parser-1.0.0-SNAPSHOT.jar`
1. Go to http://localhost:8080
